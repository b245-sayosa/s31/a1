//using require directive

const http = require('http')
// declaring port
const port = 3000
// creating server
const server = http.createServer((request, response) => {
	if(request.url == '/login') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Welcome to the login page");
	
	} else if(request.url == '/register') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("I'm sorry the page you are looking for cannot be found");


	} else {

		//Set a status code for the response - a 404 means "NOT FOUND"
		response.writeHead(404, {'Content-Type': 'text/plain'});
		response.end('ERROR 404: Page not found');
	}

});

server.listen(port);

console.log(`Server now accessible at localhost: ${port}`);